const getSum = (str1, str2) => {
    if (typeof str1 !== "string" || typeof str2 !== "string") return false;
    if (str1.length === 0) str1 = "0";
    if (str2.length === 0) str2 = "0";

    const REGEX = /^\d+$/;
    if (!REGEX.test(str1) || !REGEX.test(str2)) return false;

    const Sum = parseInt(str1) + parseInt(str2);
    return String(Sum);

}

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
    let post = 0;
    let comments = 0;

    for (const i in listOfPosts) {
        if (listOfPosts[i]["author"] === authorName) {
            post++;
        }
        for (const j in listOfPosts[i]["comments"]) {
            if (listOfPosts[i]["comments"][j]["author"] === authorName) {
                comments++;
            }

        }
    }
    return `Post:${post},comments:${comments}`;
}

const tickets = (people) => {
    let array = [];
    for (const key of people) {
        if (key === 25) {
            array.push(Number(key));
        } else {
            array.push(Number(key) - 25);
        }
    }
    let count = 0;
    for (let index = 0; index < array.length - 1; index++) {
        if (array[index] < array[index + 1] || array[0] > 25) count++;

        array[index + 1] = array[index + 1] + array[index];
    }

    if (count > 0) return "NO";
    else {
        return "YES";
    }

}


module.exports = { getSum, getQuantityPostsByAuthor, tickets };